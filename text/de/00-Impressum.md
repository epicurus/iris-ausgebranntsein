<div class="impressum">
    <strong>Bitbucket</strong>:
    <!-- if booktype is epub,mobi --><a href="${BOOK_REPOSITORY}">iris-johannson-anpassung</a><br /><!-- endif booktype -->
    <!-- if booktype is pdf --><span>${BOOK_REPOSITORY}</span><!-- endif booktype -->
    <br />
    <!-- if booktype is epub,mobi --><strong>ASIN</strong>: ${BOOK_ASIN}<br /><!-- endif booktype -->
    <!-- if booktype is pdf --><strong>ISBN</strong>: ${BOOK_ISBN}<br /><!-- endif booktype -->
    <strong>Version</strong>: ${BOOK_VERSION}.${BITBUCKET_BUILD_NUMBER}.${BITBUCKET_COMMIT}<br />
    <strong>Lizenz</strong>: ${BOOK_LICENSE}<br />
    <strong>Mitwirkende</strong>:<br />
    Marcel Desax (Übersetzung)<br />
    Michael Schmidt (Lektorat)<br />
</div>

<div class="title-page">Miteinander sprechen ... über Anpassung.</div>
<div style="visibility: hidden;">\pagebreak</div>
