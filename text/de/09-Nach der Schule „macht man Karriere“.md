## Nach der Schule „macht man Karriere“

Die meisten versuchen ihren Platz im „Etablissement“ zu finden.
Für viele wird die Anpassung sehr schwer, sie finden ihren eigenen Platz von sich heraus nicht und das Leben wird ein recht konfliktgeladener Balanceakt. Und es bedarf einer ziemlich guten Ökonomie, um mit denen Schritt zu halten, mit denen man sich messen will.
Man erfährt oft ein Gefühl, dass man nicht ausreicht, dass man etwas nicht hinkriegt, dass man die ganze Zeit Dinge abwählen muss, die man eigentlich hätte tun wollen oder fühlt, dass man sie hätte tun sollen. Das schlechte Gewissen wächst und oft taucht ein unan­genehmes Gefühl auf, dass man betrogen worden ist, man fühlt sich in einer Falle und mehr und mehr unfrei.
Um das auszuhalten sucht man nach verschiedenen Arten von Entlastung. Manchmal drückt sich das in Untreue aus, die ja oft nur eine Form von Trotz ist, wodurch man einem Gefühl von Zwang entflieht, um ein kleines bis­schen Eigenwert und Freiheit zu finden. Aber diese Form von Entlastung führt zu noch mehr Stress und zu Lügen, Doppelspiel und einer noch größeren Unfreiheit.
Oft führt das dazu, dass der Körper reagiert, und man wird krank. Der Körper gibt oft Signale in einem frühen Zeitpunkt, er gibt uns Warnungen auf viele verschiedene Weise, aber das vermögen wir nicht zu hören oder auch wollen wir es nicht hören. Manchmal geht es so weit, dass wir mit den Signalen, die der Körper gibt, nichts anfangen können, es ist so vieles um uns herum, und unser Fokus ist die ganze Zeit nach außen gerichtet, und da merken wir nicht, wie es dem Körper geht. Wir stecken so fest in dem Muster, dass wir nicht glauben, dass man die Geschwindigkeit bremsen kann und das „Rad“ anhalten kann.
Viele geraten in eine Lage, wo sie sich in eine so schwierige ökonomische Situation versetzt haben zu einem frühen Zeitpunkt, dass sie keinen Ausweg sehen. Sein Leben zu ändern und eventuell den Standard zu senken, den man so mühselig aufgebaut hat, wird als unmöglich erlebt. Das würde viel zu große Aufopferungen kosten, man wäre gezwungen das Haus zu verkaufen oder das Auto oder etwas anderes, das man als unentbehrlich ansieht, „das könnte ja den Status ändern, den man in gewissen Zusammenhängen errungen hat“.
Innezuhalten und umzudenken, zu überlegen, was das Wichtige im Leben ist, kann oft als sehr schwierig erlebt werden, und dem geht auch oft Angst voraus.
Man hat in einem forcierten Tempo gelebt, ist rum­ge­flitzt, hat alles geschmissen und geordnet, was nötig war, war die ganze Zeit gezwungen, schnelle Entschlüsse zu fassen und das eine auf Kosten des anderen zu wählen. Immerzu wichtige Einsätze, die man schnell ausführen musste, gleichzeitig wie es viele verschiedene „Lebens­notwendigkeiten“ gab, um die man sich kümmern musste.
Äußerst selten hat man innegehalten und über das Leben nachgedacht und auf welchem Weg man sich befindet, wo unsere ganze moderne Gesellschaft hingeht, weshalb wir alles tun, was wir tun, für wen wir es eigentlich tun, wer eigentlich davon profitiert. Wir leben unter einem Tüchtigkeitskomplex und wagen nichts anderes, denn dann … das Gefühl von Katastrophe lauert irgendwo da hinten im Nacken und treibt uns weiter.
Es werden ständig verschiedene Untersuchungen darüber ausgeführt, wie es den Menschen geht. Man hat auch untersucht, wie viele sich unausgeglichen fühlen und andauernd ein schlechtes Gewissen haben, weil sie meinen, nicht auszureichen. Man meint, dass man seinen Mann/Frau und seine Kinder enttäuscht und dass man sich nicht genug engagiert in das, was die Kinder tun, auch geht man umher mit einem schlechten Gewissen, weil man sich nicht genug um seine alternden Eltern kümmert, usw. Gemäß Statistik gilt das für 60% der Bevölkerung. Die Untersuchung wurde mit Erwachsenen zwischen 25-60 Jahren aus beiden Geschlechtern durchgeführt.
Das kann unmöglich der Fehler eines so großen Teiles der Bevölkerung sein. Worin liegt denn aber der Fehler? Wenn der Fehler nicht beim einzelnen Menschen liegt, wo können wir ihn denn finden und was ist es, was falsch ist?
Betrachtet man die Problematik mit genügendem Ab­stand, sieht man deutlich, dass es sich hier um einen „Denkfehler“ handelt. Der eigentliche Ausgangspunkt in unserem Denken ist falsch. Wir meinen einen Lebensstil kaufen zu können, Glück kaufen zu können oder glau­ben, dass das Glück wie eine Gabe von oben zu uns kommt. Es fühlt sich undenkbar an, dass die Gesellschaft ohne Gewinninteressen und „freie Konkurrenz“ funktio­nieren könnte.
Denkfehler, die in Anpassung und Konsumieren führen – „das Richtige zu tun“.
Es sei wichtig, genau das Richtige zu tun, danach zu suchen und nicht aufzugeben bis man es erreicht hat.
Im Unterschied dazu zu denken, dass „alles was nicht falsch ist, richtig ist“. Es gilt, die Fehler, auf die man stößt, zu eliminieren, so wird es richtig.
Probiere das in der Wirklichkeit und entdecke den Unter­schied in deinem eigenen Körpererleben.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

Wie würdest du Ausgebranntsein erklären wollen?
Woher kommt es, wie entsteht es?
Glaubst du, dass es ein Fehler oder eine Schwäche im physischen Körper ist, die diesen Zustand auslösen oder glaubst du, dass es die Art des Körpers ist, auf eine falsche Einstellung oder ein falsches Denken zu reagieren?
Falls du davon betroffen wurdest, was bist du selber bereit zu tun, um davon weg zu kommen auf eine gesunde Weise?
Wenn du jemanden in deiner Nähe hast, der betroffen worden ist, was möchtest du dieser Person sagen und wie glaubst du, dass du es sagen kannst, ohne eine Verteidigungs­reaktion auszulösen?

</div>

- Viel Glück
  Iris Johansson
  Wenn Sie eine Konsultation wünschen, rufen Sie bitte die Nummer +46 708 878688 an oder mailen Sie an: iris@irisjohansson.com.
