## Können wir Kinder Kinder sein lassen?

Wenn Kinder einen solchen kommunikativen Umgang während der ersten sechs Jahre erhalten könnten, gerne auch zuhause, aber vor allem in der Kinderfürsorge, dass diejenigen, die im Kindergarten arbeiten nicht Pädagogen sind, dann würde das Selbstgefühl weitgehend bewahrt bleiben und die Kinder könnten Forderungen, Erwar­tungen und Notwendiges auf autonome Weise zu ihrer eigenen Sache umgestalten. Dann würden ihr Charakter und ihre Persönlichkeit ein vollwertiges Maß an Platz bekommen um aufzublühen.
Das beinhaltet nicht, dass die Erwachsenen passiv sein sollen. Erwachsene haben es nötig, aktiv handelnd, organisierend, kreativ und schöpferisch zu sein. Erwach­sene sollen für die unbedingte Fürsorge und die unbe­dingte Kontrolle stehen, die notwendig sind, damit die „Beelterung“ funktionieren kann, so dass die Umgebung als vertrauenswürdig und geborgen erlebt werden kann und dass da immer jemand da ist, der eingreifen kann, so lange der Selbsterhaltungstrieb und die Fähigkeit, eine gewisse Situation zu bemeistern, noch nicht entwickelt sind.
Das bedeutet, dass man der Fähigkeit der Kinder ihre eigenen Probleme zu lösen, selbst zufrieden werden zu können und selber Dinge zu lernen, wo man nur zusieht, dass die Information um sie herum vorhanden ist, nicht misstraut. Kinder müssen gestoppt werden – aber ohne die Irritation der Erwachsenen – und dann wieder freigelassen werden. Gibt es eine äußere Geborgenheit und ein Vorbild, davon wie man tut, dann können Kinder den Zustand Liebe viel leichter als ein Gefühl hochkommen lassen. Ein gutes und wahres Vorbild zu sein, ist immer der beste Lehrmeister und falls wir in Übereinstimmung mit dem, was primär waltet, leben, geben wir den Kin­dern das, was sie brauchen um sich zu freien und selbst­ständigen Individuen zu entwickeln.

<div class="handwritten">
GESPRÄCH – GEDANKEN – ÜBERLEGUNGEN<br /><br />

Wie würde die Schule aussehen, wenn die Kinder, die dorthin kämen, kreativ und schöpferisch, offen, kommunikativ, neu­gierig und forschend wären?
Wie glaubst du, dass die Aufgabe des Lehrers dann aussehen würde?
Wenn derjenige, der Lehrer ist, er selbst sein könnte in seiner Aufgabe als Lehrer, wie würde sich das von dem heute unter­schie­den?
Würde die Gesellschaft, also wir, alle freien, autonomen, einzigartigen Charaktere dul­den, die dann unsere Umgebung ausmachen und die politische Entwicklung beeinflussen würden.
Kannst du eine Wahrnehmung von auto­nomer Zusammenarbeit kontra Anpassung und Kompromiss erfühlen?

</div>
